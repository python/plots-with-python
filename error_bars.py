#!/usr/bin python

import numpy as np
import matplotlib.pyplot as plt

import form

#### Plotting
fig, axes = plt.subplots(1,2,figsize=(10,4))
ax = axes[0]

x = np.arange(10)
y = 2.5 * np.sin(x / 20 * np.pi)
yerr = np.linspace(0.05, 0.2, 10)

ax.errorbar(x, y + 3, yerr=yerr, label='both limits (default)')
ax.errorbar(x, y + 2, yerr=yerr, uplims=True, label='uplims=True')
ax.errorbar(x, y + 1, yerr=yerr, uplims=True, lolims=True, label='uplims=True, lolims=True')
upperlimits = [True, False] * 5
lowerlimits = [False, True] * 5
ax.errorbar(x, y, yerr=yerr, uplims=upperlimits, lolims=lowerlimits, label='subsets of uplims and lolims')
ax.legend(loc='lower right')

ax = axes[1]
x = np.arange(10) / 10
y = (x + 0.1)**2

ax.errorbar(x, y, xerr=0.1, xlolims=True, label='xlolims=True')
y = (x + 0.1)**3

ax.errorbar(x + 0.6, y, xerr=0.1, xuplims=upperlimits, xlolims=lowerlimits, label='subsets of xuplims and xlolims')

y = (x + 0.1)**4
ax.errorbar(x + 1.2, y, xerr=0.1, xuplims=True, label='xuplims=True')
ax.legend()

plt.savefig("error_bars.pgf", format='pgf')
    # then \renewcommand\ssfamily{} \input{error_bars.pgf}

#plt.savefig("error_bars.eps", format='eps')
#plt.savefig("error_bars.png", dpi=200) # this is a high-res dpi

plt.show()

