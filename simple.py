#!/usr/bin python

import numpy as np
import matplotlib.pyplot as plt

import form

#### Plotting
x = np.linspace(0,15,30)
y = np.sin(x) + 0.1*np.random.randn(len(x))

plt.figure(figsize=(8,3))
plt.plot(x,y, 'o--', color='purple', lw=2, ms=10)

plt.xlabel("Time t [s]")
plt.ylabel("Voltage V(t) [V]")
#plt.title("Voltage vs times")

plt.savefig("simple.pgf", format='pgf') 
    # then \renewcommand\ssfamily{} \input{simple.pgf}

#plt.savefig("simple.eps", format='eps')
#plt.savefig("simple.png", dpi=200)

plt.show()
