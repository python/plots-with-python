import matplotlib.pyplot as plt
import scienceplots

### Locale: to french (decimal comma)
#import locale
#locale.setlocale(locale.LC_NUMERIC, "fr_FR.UTF-8")
#plt.rcParams['axes.formatter.use_locale'] = True

### Format
plt.style.use(['science','grid'])
FONT_SIZE = 12
plt.rc('font', size=FONT_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=FONT_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=FONT_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=FONT_SIZE-2)    # fontsize of the tick labels
plt.rc('ytick', labelsize=FONT_SIZE-2)    # fontsize of the tick labels
plt.rc('legend', fontsize=FONT_SIZE)    # legend fontsize
plt.rc('figure', titlesize=FONT_SIZE)  # fontsize of the figure title
###
