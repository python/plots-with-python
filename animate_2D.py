#!/usr/bin python

import numpy as np
import matplotlib.pyplot as plt

from matplotlib import animation
from matplotlib.animation import PillowWriter

import form

#### Plotting
def f(x,t):
    return np.sin(x-3*t)

x = np.linspace(0, 10*np.pi, 1000)
t = np.arange(0, 24, 1/60)
X, T = np.meshgrid(x, t)
F = f(X,T)

fig, ax = plt.subplots(1,1, figsize=(8,4))
ln1, = plt.plot([], [])
time_text = ax.text(0.65, 0.95, '', fontsize=15, bbox=dict(facecolor='white', edgecolor='black'), transform=ax.transAxes)
ax.set_xlim(0,10*np.pi)
ax.set_ylim(-1.5, 1.5)

def animate(i):
    ln1.set_data(x, F[i])
    time_text.set_text('t={:.2f}'.format(i/60))
    #plt.savefig('animate_2D-{}.png'.format(i))
        #then with animate: \animategraphics[autoplay,loop,width=.6\linewidth]{12}{animate_2D-}{0}{239}

ani = animation.FuncAnimation(fig, animate, frames=240, interval=50)
ani.save('animate_2D.gif',writer='pillow',fps=50,dpi=100)
    # see https://holypython.com/how-to-save-matplotlib-animations-the-ultimate-guide/ for more vid formats

plt.show()
