#!/usr/bin python3

import numpy as np
import matplotlib.pyplot as plt

import form

A, H = 1e-4, 1e3
theta0 = 300

metals = np.array([('Cu', 3.45e7, 1.11e-4), ('Fe', 3.50e7, 2.3e-5)], dtype=[('symbol', '|S2'), ('cp', 'f8'), ('D', 'f8')])

xlim, nx = 0.05, 1024
x = np.linspace(-xlim, xlim, nx)

times = (1e-2, 0.1, 1)

fig, axes = plt.subplots(3, 2, figsize=(7,8))
for j,t in enumerate(times):
    for i, metal in enumerate(metals):
        symbol, cp, D = metal
        ax = axes[j,i]

        theta = theta0 + H/cp/A/np.sqrt(D*t * 4*np.pi) * np.exp(-x**2/4/D/t)

        ax.plot(x*100, theta)
        ax.set_title('{}, $t={}$ s'.format(symbol.decode('utf8'), t))
        ax.set_xlim(-4,4)
        ax.set_xlabel('$x\;[\mathrm{cm}]$')
        ax.set_ylabel('$\Theta\;[\mathrm{K}]$')

for j in range(3):
    ymax = max(axes[j,0].get_ylim()[1], axes[j,1].get_ylim()[1])
    for i in range(2):
        ax = axes[j,i]
        ax.set_ylim(theta0,ymax)
        ax.set_yticks([theta0, (ymax+theta0)/2, ymax])

fig.tight_layout()
plt.show()
