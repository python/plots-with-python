#!/usr/bin python

import numpy as np
import matplotlib.pyplot as plt

import form

#### Plotting
x = np.linspace(0,15,30)
y = np.sin(x) + 0.1*np.random.randn(len(x))

fig, ax = plt.subplots(1, 1, figsize=(12,4))
ax.plot(x, y, 'o--', color='r', lw=0.4, ms=3)
ax.text(0.1, 0.1, 'text here', transform=ax.transAxes) # tranform arg to switch into absolute coordinates
ax.set_xlabel("Time [s]")
ax.set_ylabel(r'$\frac{d}{d x} f(x)$')
#ax.set_title("title")

plt.savefig("text_on_figure.pgf", format='pgf') 
    # then \renewcommand\ssfamily{} \input{text_on_figure.pgf}

#plt.savefig("text_on_figure.eps", format='eps')
#plt.savefig("text_on_figure.png", dpi=200) # this is a high-res dpi

plt.show()

