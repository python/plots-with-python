#!/usr/bin python

import numpy as np
import matplotlib.pyplot as plt

import form

#### Plotting
_ = np.linspace(-1, 1, 100)
x, y = np.meshgrid(_,_)
z = x**2+x*y

plt.contourf(x,y,z, levels=50, cmap='plasma')
# see https://matplotlib.org/stable/tutorials/colors/colormaps.html#sphx-glr-tutorials-colors-colormaps-py for colors
plt.colorbar(label="Temperature [$^\circ C$]")
plt.xlabel("Horizontal Position [m]")
plt.ylabel("Vertical Position [m]")
#plt.title("title")

plt.savefig("simple_2D_colors.pgf", format='pgf') 
    # then \renewcommand\ssfamily{} \input{simple_2D_colors.pgf}

#plt.savefig("simple_2D_colors.eps", format='eps')
#plt.savefig("simple_2D_colors.png", dpi=200) # this is a high-res dpi

plt.show()
